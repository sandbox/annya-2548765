<?php
// NONE value for RDEP settings for each field instance.
define('RDEP_NONE_VALUE', 'none');

/**
 * Implements hook_references_dialog_widgets_alter().
 */
function rdep_references_dialog_widgets_alter(&$widgets) {
  $widgets['entityreference_autocomplete']['operations']['add_prepopulate'] = array(
    'function' => 'rdep_references_dialog_entityreference_add_prepopulate_link',
    'title' => t('Add dialog with prepopulate functionality'),
  );
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function rdep_form_field_ui_field_edit_form_alter(&$form, $form_state) {
  if ($form['instance']['widget']['type']['#value'] == 'entityreference_autocomplete') {
    $field = $form['#field'];
    $instance = field_info_instance($form['instance']['entity_type']['#value'], $form['instance']['field_name']['#value'], $form['instance']['bundle']['#value']);
    if (isset($form['instance']['widget']['settings'])) {
      $form['instance']['widget']['settings'] += rdep_settings_form($field, $instance);
    }
    else {
      $form['instance']['widget']['settings'] = rdep_settings_form($field, $instance);
    }
  }
}

/**
 * A widget settings form for our references dialog fields.
 */
function rdep_settings_form($field, $instance) {
  $widget = $instance['widget'];
  $defaults = field_info_widget_settings($widget['type']);
  $settings = array_merge($defaults, $widget['settings']);
  $er_fields = field_read_fields(array('type' => 'entityreference'));

  // Add our own additions if Add prepopulate checkbox is checked.
  if (empty($settings['references_dialog']['add_prepopulate'])) {
    return array();
  }

  $form['rdep'] = array(
    '#type' => 'fieldset',
    '#title' => t('RDEP settings'),
    '#description' => t('Choose relations between entity relations fields of entity related bundles and current bundle entity reference field.')
  );

  $er_instance_fields = field_read_instances(array(
    'entity_type' => $instance['entity_type'],
    'bundle' => $instance['bundle'],
    'field_name' => array_keys($er_fields)
  ));

  $options = array(
    RDEP_NONE_VALUE => t('- None -'),
    'id' => t('Entity identifier'),
  );

  $er_fields = field_read_instances(array(
    'entity_type' => $field['settings']['target_type'],
    'bundle' => $field['settings']['handler_settings']['target_bundles'],
    'field_name' => array_keys($er_fields)
  ));

  foreach($er_instance_fields as $field_instance) {
    $options[$field_instance['field_name']] = $field_instance['label'];
  }

  // Go throw entity reference fields and add ability to choose referenced between bundle fields and entityreference fields.
  foreach($er_fields as $field_instance) {
    $form['rdep'][$field_instance['entity_type']][$field_instance['bundle']][$field_instance['field_name']] = array(
      '#type' => 'select',
      '#title' => check_plain($field_instance['label'] . '(' . $field_instance['bundle'] . ')'),
      '#options' => $options,
      '#default_value' => rdep_settings_form_default_values($settings, $field_instance),
    );
  }

  return $form;
}

/**
 * Add link callback for entity references with prepopulate functionality.
 */
function rdep_references_dialog_entityreference_add_prepopulate_link($element, $widget_settings, $field, $instance) {
  // Hide add link for non-empty default value.
  if (!empty($element['#default_value']) || !empty($element['#value'])) {
    return array();
  }

  $add_links = array();
  $entity_type = $field['settings']['target_type'];
  $entity_info = entity_get_info($entity_type);
  $entity_bundles = array_keys($entity_info['bundles']);
  if (!empty($field['settings']['handler_settings']['target_bundles'])) {
    $bundles = $field['settings']['handler_settings']['target_bundles'];
  }
  elseif (!empty($field['settings']['handler_settings']['view'])) {
    $name = $field['settings']['handler_settings']['view']['view_name'];
    $display = $field['settings']['handler_settings']['view']['display_name'];
    $views = views_get_view($name);
    $views_display = $views->display;

    if (isset($views_display[$display]->display_options['filters']['type'])) {
      $views_filters = $views_display[$display]->display_options['filters']['type']['value'];
    }
    else {
      $views_filters = $views_display['default']->display_options['filters']['type']['value'];
    }

    $bundles = array_keys($views_filters);
  }
  elseif (isset($entity_info['bundles'])) {
    // If the entity target bundles is empty, it means all target bundles are allowed. Fill it all up!
    $bundles = $entity_bundles;
  }
  // Create a link for each allowed bundles.
  if (isset($bundles)) {
    foreach ($bundles as $bundle) {
      if (in_array($bundle, $entity_bundles) && $link = rdep_entityreference_link_helper($entity_type, $bundle, $element, $instance)) {
        $add_links[] = $link;
      }
    }
  }
  return $add_links;
}

/**
 * Return link for Add prepopulate.
 */
function rdep_entityreference_link_helper($entity_type, $bundle, $populate_entity, $instance) {
  $rdep = $instance['widget']['settings']['rdep'];
  $wrapper = entity_metadata_wrapper($entity_type, NULL, array('bundle' => $bundle));
  $populate_wrapper = entity_metadata_wrapper($populate_entity['#entity_type'], $populate_entity['#entity']);

  $info = $wrapper->entityInfo();
  if (isset($bundle)) {
    $label = $info['bundles'][$bundle]['label'];
  }
  else {
    $label = $info['label'];
  }

  // entity_access() doesn't provide a generic bundle create op access check.
  switch ($entity_type) {
    case 'node':
      $access = node_access('create', $bundle);
      break;

    default:
      $access = entity_access('create', $entity_type);
      break;
  }

  $path = references_dialog_get_admin_path($entity_type, 'add', $bundle);

  if ($access && $path) {
    $link = array(
      'title' => t('Create @type', array('@type' => $label)),
      'href' => $path,
    );

    // Fill query with prepopulate values according to field instance settings.
    foreach($rdep as $key => $setting) {
      if (!empty($rdep[$entity_type][$bundle])) {
        foreach($rdep[$entity_type][$bundle] as $key => $value) {
          $data = array();
          if ($value != RDEP_NONE_VALUE) {
            if ($value == 'id') {
              $data[] = $populate_wrapper->getIdentifier();
            }
            else{
              if (isset($populate_wrapper->$value)) {
                $result = $populate_wrapper->$value->value();
                if (is_array($result)) {
                  foreach ($populate_wrapper->$value->getIterator() as $delta => $value_wrapper) {
                    if ($id = $value_wrapper->getIdentifier()) {
                      $data[] = $id;
                    }

                  }
                }
                else {
                  if ($id = $populate_wrapper->$value->getIdentifier()) {
                    $data[] = $populate_wrapper->$value->getIdentifier();
                  }
                }
              }
            }

            if (!empty($data)) {
              $link['query'][$key] = implode(',', $data);
            }
          }
        }
      }
    }

    return $link;
  }
  return FALSE;
}

/**
 * Get default values for RDEP settings.
 */
function rdep_settings_form_default_values($settings, $field) {
  $entity_type = $field['entity_type'];
  $bundle = $field['bundle'];
  $field_name = $field['field_name'];

  if (!empty($settings['rdep']) && !empty($settings['rdep'][$entity_type])
      && !empty($settings['rdep'][$entity_type][$bundle])
      && !empty($settings['rdep'][$entity_type][$bundle][$field_name])) {

    return $settings['rdep'][$entity_type][$bundle][$field_name];
  }
  return RDEP_NONE_VALUE;
}
